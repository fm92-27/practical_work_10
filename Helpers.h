int SumSquared(int x, int y)
{
	double SquaredX = pow(x, 2);
	double SquaredY = pow(y, 2);
	return (SquaredX + SquaredY);
}